This is embedded application to start timer and send output to Raspberry Pi.

We have to create a jar file which will be executed on IOIO OTG board

Connect board on USB port of computer and later it will be connected to Pi Board.



Download the samples from https://github.com/ytai/ioio/wiki/Downloads

I am using APP-IOIO0503.zip which is 5.00/5.03 version

Import projects in eclipse then open HelloIOIOConsole.java file in HelloIOIOConsole 
project. Then replace this file the same file present in repository.

OR

Import projects from Repository, Add Library then try to run on Pi Board.